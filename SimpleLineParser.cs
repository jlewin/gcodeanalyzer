﻿/*
Copyright (c) 2015, John Lewin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
*/

using MatterHackers.VectorMath;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GCodeAnalyzer
{
	public class GCodeLayer
	{
		public GCodeLayer()
		{
			Moves = new List<Line>();
		}

		public List<Line> Moves { get; set; }

		public IEnumerable<Line> ExtrudedMoves => Moves.Where(m => m.Start.Command == "G1");

		public Vector3[] Points()
		{
			List<Vector3> points = new List<Vector3>();

			// Add the starting point
			points.Add(Moves.First().Start.Point);

			foreach (var line in Moves)
			{
				// and the ending point of every line
				points.Add(line.End.Point);
			}

			return points.ToArray();
		}
	} 

	public class LineParser
	{
		public static List<GCodeLayer> LoadFile(string outputPath)
		{
			string[] allLines = File.ReadAllLines(outputPath);

			Instruction lastMove = null;

			var layers = new List<GCodeLayer>();
			var activeLayer = new GCodeLayer();

			foreach (string line in allLines.Select(l => l.Trim()))
			{
				if (line.StartsWith("; LAYER:"))
				{
					activeLayer = new GCodeLayer();
					layers.Add(activeLayer);
				}
				else if (line.StartsWith(";"))
				{
					// Skip comment lines
					continue;
				}

				// Drop comments
				
				var instruction = new Instruction(line);

				if(instruction.ExtrudeOnly)
				{
					continue;
				}
				else if (instruction.Command == "G0")
				{
					if (lastMove != null)
					{
						activeLayer.Moves.Add(new Line(lastMove, instruction));
					}

					lastMove = instruction;
				}
				else if (instruction.Command == "G1")
				{
					if (lastMove != null)
					{
						activeLayer.Moves.Add(new Line(lastMove, instruction));
					}

					lastMove = instruction;
				}
			}

			return layers;
		}
	}

	public class PointDouble
	{
		public double X { get; set; }
		public double Y { get; set; }
	}

	public class Line
	{
		public Instruction Start { get; set; }
		public Instruction End { get; set; }
		public bool Extrude { get; set; }
		public PointDouble Midpoint { get; set; }

		public double Slope { get; set; }

		public Line(Instruction start, Instruction end)
		{

			Start = start;
			End = end;
			Extrude = start.E != null || end.E != null;

			Midpoint = new PointDouble()
			{
				X = (start.X + end.X) / 2,
				Y = (start.Y + end.Y) / 2,
			};

			Slope = (start.Y - end.Y) / (start.X - end.X);
		}

		public override string ToString()
		{
			return string.Format("{7} {0},{1} to {2},{3} - ({4:0.##},{5:0.##}) [{6}]",
				Start.X,
				Start.Y,
				End.X,
				End.Y,
				Midpoint.X,
				Midpoint.Y,
				Slope,
				Start.Command);
		}
	}

	public class Instruction
	{
		public string Command { get; private set; }
		public double X { get { return Point.x; } }
		public double Y { get { return Point.y; } }
		public double Z { get { return Point.z; } }
		public string F { get; private set; }
		public string E { get; private set; }
		public bool ExtrudeOnly { get; set; }
		public string[] CommandSegments { get; private set; }

		public Vector3 Point { get; private set; }

		public static double RunningZ { get; private set; }

		public Instruction(string line)
		{
			int pos = line.IndexOf(";");
			string lineClipped = pos == -1 ? line : line.Substring(0, pos);
			CommandSegments = Regex.Split(lineClipped.Trim(), "\\s+");

			this.ExtrudeOnly = true;
			this.E = null;
			this.Command = CommandSegments[0];

			double x = 0, y = 0;

			foreach (var v in CommandSegments.Skip(1))
			{
				string prop = v.Substring(0, 1);
				string val = v.Substring(1);

				switch (prop)
				{
					case "X":
						x = double.Parse(val);
						this.ExtrudeOnly = false;
						break;

					case "Y":
						y = double.Parse(val);
						this.ExtrudeOnly = false;
						break;

					case "Z":
						RunningZ = double.Parse(val);
						break;

					case "F":
						this.F = val;
						break;
					case "E":
						this.E = val;
						break;
				}
			}

			this.Point = (ExtrudeOnly) ? new Vector3() : new Vector3(x, y, RunningZ);
		}

		public override string ToString()
		{
			return string.Format($"[{X:0.000000}, {Y:0.000000}, {Z:0.000000}],");
		}
	}
}
